# Tech-Revealed

This project is composed of static HTML and CSS files.

## How to use the project:

**Step 1**:
Clone the git repository 

**Step 2**:
*Option 1* : User can choose to deploy the project in a web server such as Apache HTTP server.
*Option 2* : Simply open `default.html` file in a latest web-browser.

License
Copyright (c) 2019 Samreen Kaur

Tech-Revealed is released under the MIT license.